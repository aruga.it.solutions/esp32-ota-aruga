/*
 * TriggerCode.ino - Simple example code for EasyNextionLibrary
 * Copyright (c) 2020 Athanasios Seitanis < seithagta@gmail.com >. 
 * https://www.seithan.com 
 * All rights reserved. EasyNextionLibrary is licensed under the MIT License
 * https://opensource.org/licenses/MIT
 */
 
/* I have invested time and resources providing open source codes, like this one. 
 * Please do not hesitate to support my work!
 * If you found  this work useful and has saved you time and effort,
 * Just simply paypal me at: seithagta@gmail.com
 */
 
 //********************************************************************************
 //  You can find more examples, tutorials and projects with Nextion on my website
 //  https://www.seithan.com 
 //********************************************************************************
 
// Compatible for Arduino

/* This is the most important method of the library. 
 * And this is because, it gives you the ability to use the predefined functions and run your code from there. 
 * These predefined functions are named trigger0(), trigger1(), trigger2()... up to trigger50(). 
 * You can use them as a simple void out of the loop, in which you will have written a block of code to run every time it is called.
 * You can call those trigger() functions and run the code they contain anytime by simply writing in a Nextion Event the command:
 * `printh 23 02 54 XX` , where `XX` the id for the triggerXX() in HEX.
 * Example: printh 23 02 54 00 to call trigger0() ... printh 23 02 54 0A to call trigger10() and so on...
 */

/*
  Declare the void by simply writing:
  void trigger0(){
  [ put your code here !!!!]
  }
*/

#include <WiFi.h>
#include <HTTPClient.h>
#include <HTTPUpdate.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include "cert.h"
#include "Nextion.h"
#include "EasyNextionLibrary.h"

EasyNex myNex(Serial); // Create an object of EasyNex class with the name < myNex >


const char * ssid = "infra_srv_1RL";
const char * password = "j4MpaDTS";
const char* mqtt_server = "broker.hivemq.com";

IPAddress ip;

#define DHTPIN 14 
#define DHTTYPE    DHT21     // DHT 21 (AM2301)
DHT_Unified dht(DHTPIN, DHTTYPE);
uint16_t currentPage;

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

void callback(char* topic, byte* payload, unsigned int length) {
  //Serial.print("Message arrived [");
  //Serial.print(topic);
  //Serial.print("] ");
  for (int i = 0; i < length; i++) {
    //Serial.print((char)payload[i]);
  }
  //Serial.println();

}

String FirmwareVer = {
  "2.4.9"
};
#define URL_fw_Version "https://gitlab.com/aruga.it.solutions/esp32-ota-aruga/-/raw/main/esp32_ota/bin_version.txt"
#define URL_fw_Bin "https://gitlab.com/aruga.it.solutions/esp32-ota-aruga/-/raw/main/esp32_ota/fw.bin"


void connect_wifi();
int firmwareUpdate();
int FirmwareVersionCheck();

unsigned long previousMillis = 0; // will store last time LED was updated
unsigned long previousMillis_2 = 0;
const long interval = 60000;
const long mini_interval = 1000;

String page="0";

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    
    //Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      //Serial.println("connected");
      // Once connected, publish an announcement...
      //client.publish("outTopic", "hello world");
      // ... and resubscribe
      //client.subscribe("inTopic");
    } else {
      //Serial.print("failed, rc=");
      //Serial.print(client.state());
      //Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void setup(){
  myNex.begin(9600); // Begin the object with a baud rate of 9600
  //Serial.begin(9600); // Begin the object with a baud rate of 9600
                     // If no parameter was given in the begin(), the default baud rate of 9600 will be used
  
  pinMode(13, OUTPUT); // The built-in LED is initialized as an output
  digitalWrite(13, LOW);
  dht.begin();
  //Serial.print("Active firmware version:");
  //Serial.println(FirmwareVer);
  connect_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  myNex.writeStr("pageNum.txt", String(FirmwareVer));
  delay(3000);
}

void loop(){
  myNex.NextionListen(); // WARNING: This function must be called repeatedly to response touch events
  myNex.writeStr("pageNum.txt", page);

  if (!client.connected()) {
    
    reconnect();
  }
 
  unsigned long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    String jsonData = deviceJsonData();
    client.publish("Aruga-111", "Connected...");
    client.publish("Aruga/device2", jsonData.c_str());
  }

  client.loop();

} 


void connect_wifi() {
  //Serial.println("Waiting for WiFi");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    //Serial.print(".");
  }

  //Serial.println("");
  //Serial.println("WiFi connected");
  //Serial.println("IP address: ");
  //Serial.println(WiFi.localIP());
}


int firmwareUpdate(void) {
  WiFiClientSecure client;
  client.setCACert(rootCACertificate);
  httpUpdate.setLedPin(13, LOW);
  t_httpUpdate_return ret = httpUpdate.update(client, URL_fw_Bin);

  switch (ret) {
  case HTTP_UPDATE_FAILED:
    //Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s\n", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
    myNex.writeStr("page.txt", String("Fail"));
    return 0;
    break;

  case HTTP_UPDATE_NO_UPDATES:
    //Serial.println("HTTP_UPDATE_NO_UPDATES");
    myNex.writeStr("page.txt", String("NO"));
    return 1;
    break;

  case HTTP_UPDATE_OK:
    //Serial.println("HTTP_UPDATE_OK");
    myNex.writeStr("page.txt", String("OK"));
    return 2;
    break;
  }
}
int FirmwareVersionCheck(void) {
  String payload;
  int httpCode;
  String fwurl = "";
  fwurl += URL_fw_Version;
  fwurl += "?";
  fwurl += String(rand());
  Serial.println(fwurl);
  WiFiClientSecure * client = new WiFiClientSecure;

  if (client) 
  {
    client -> setCACert(rootCACertificate);

    // Add a scoping block for HTTPClient https to make sure it is destroyed before WiFiClientSecure *client is 
    HTTPClient https;

    if (https.begin( * client, fwurl)) 
    { // HTTPS      
      //Serial.print("[HTTPS] GET...\n");
      // start connection and send HTTP header
      delay(100);
      httpCode = https.GET();
      delay(100);
      if (httpCode == HTTP_CODE_OK) // if version received
      {
        payload = https.getString(); // save received version
      } else {
        myNex.writeStr("page.txt", String("error"));
        //Serial.print("error in downloading version file:");
        //Serial.println(httpCode);
      }
      https.end();
    }
    delete client;
  }
      
  if (httpCode == HTTP_CODE_OK) // if version received
  {
    payload.trim();
    if (payload.equals(FirmwareVer)) {
      //Serial.printf("\nDevice already on latest firmware version:%s\n", FirmwareVer);
     myNex.writeStr("page.txt", String("still"));
      return 0;
    } 
    else 
    {
      //Serial.println(payload);
      //Serial.println("New firmware detected");
      myNex.writeStr("page.txt", String("new fw"));
      return 1;
    }
  } 
  return 0;  
}

String deviceJsonData() 
{  
   
   sensors_event_t event;
   dht.temperature().getEvent(&event);
   float temp = event.temperature;
   dht.humidity().getEvent(&event);
   float humid = event.relative_humidity;
   ip = WiFi.localIP();
   String data = "{";
      data+=" \"temp\":\"";
      data+=String(temp);
      data+="\",";
      data+=" \"humid\":\"";
      data+=String(humid);
      data+="\",";
      data+=" \"mac\":\"";
      data+=String(WiFi.macAddress());
      data+="\",";
      data+=" \"IP\":\"";
      data+=String(ip);
      data+="\"}";
  //Serial.println(data);
  return data;
}


  // To call this void send from Nextion's component's Event:  printh 23 02 54 00
void trigger0(){

  // In this exmaple, we send this command from the Release Event of b0 button (see the HMI of this example)
  // You can send  the same `printh` command, to call the same function, from more than one component, depending on your needs

  digitalWrite(13, !digitalRead(13)); // If LED_BUILTIN is ON, turn it OFF, or the opposite
  if(digitalRead(13) == HIGH){
    myNex.writeNum("switch.bco", 2016); // Set button b0 background color to GREEN (color code: 2016)
    myNex.writeStr("switch.txt", "ON"); // Set button b0 text to "ON"
      
  }else if(digitalRead(13) == LOW){
    myNex.writeNum("switch.bco", 63488); // Set button b0 background color to RED (color code: 63488)
    myNex.writeStr("switch.txt", "OFF"); // Set button b0 text to "OFF"
   
  }
}

  // To call this void send from Nextion's component's Event:  printh 23 02 54 01
void trigger1(){
    currentPage = 0;
    page = String(currentPage);
  
  }
  // To call this void send from Nextion's component's Event:  printh 23 02 54 02
void trigger2(){
    currentPage = 1;
    page = String(currentPage);
    
  }
    // To call this void send from Nextion's component's Event:  printh 23 02 54 03
void trigger3(){
    currentPage = 2;
    page = String(currentPage);
    
  }
      // To call this void send from Nextion's component's Event:  printh 23 02 54 04
void trigger4(){
    currentPage = 3;
    page = String(currentPage);
    
    
  }
// To call this void send from Nextion's component's Event:  printh 23 02 54 05
void trigger5(){
    //myNex.writeStr("page.txt", "Updating..");
    digitalWrite(13, !digitalRead(13)); // If LED_BUILTIN is ON, turn it OFF, or the opposite
  if(digitalRead(13) == HIGH){
    myNex.writeNum("switch.bco", 2016); // Set button b0 background color to GREEN (color code: 2016)q
    myNex.writeStr("switch.txt", "ON"); // Set button b0 text to "ON"
      
  }else if(digitalRead(13) == LOW){
    myNex.writeNum("switch.bco", 63488); // Set button b0 background color to RED (color code: 63488)
    myNex.writeStr("switch.txt", "OFF"); // Set button b0 text to "OFF"
   
  }
   myNex.writeStr("page.txt", "updating...");
    if (FirmwareVersionCheck()) {
      myNex.writeStr("page.txt", "New FW");
      if(firmwareUpdate()==2){
        myNex.writeStr("page.txt", "upgraded");
      }
    }
    myNex.writeStr("page.txt", "updated");
    
  }
